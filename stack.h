#ifndef __STACK__
#define __STACK__

// https://stackoverflow.com/questions/10636120/check-if-bool-is-defined-in-mixed-c-c
#ifndef __cplusplus
typedef unsigned char bool;
static const bool false = 0;
static const bool true = 1;
#endif

#define MAXSIZE 46

#include <limits.h>

/**
 * @brief The implementation of a stack
 *
 */
typedef struct __Stack Stack;

/**
 * @brief An interface structure to represent a stack
 *
 */
struct __Stack {
    int top;
    unsigned int *array;
    unsigned int size;
    /**
     * @brief Function to remove an item from stack. It decreases top by 1.
     *
     */
    unsigned int (*Pop)(Stack *);
    /**
     * @brief Function to add an item to stack. It increases top by 1.
     *
     */
    void (*Push)(Stack *, int);
    /**
     * @brief Function to return the top from stack without removing it.
     *
     */
    unsigned int (*Peek)(Stack *);
    /**
     * @brief Stack is empty when top is equal to -1.
     *
     */
    bool (*IsEmpty)(Stack *);
    /**
     * @brief Stack is full when top is equal to the last index.
     *
     */
    bool (*IsFull)(Stack *);
};

/**
 * @brief
 *
 * @param unsigned int
 * @return Stack*
 */
Stack *New(unsigned int);

/**
 * @brief Function to remove an item from stack. It decreases top by 1.
 *
 * @param stack
 * @return unsigned int
 */
unsigned int Pop(Stack *stack);

/**
 * @brief Function to add an item to stack. It increases top by 1.
 *
 * @param stack
 * @param item
 */
void Push(Stack *stack, int item);

/**
 * @brief Function to return the top from stack without removing it.
 *
 * @param stack
 * @return unsigned int
 */
unsigned int Peek(Stack *stack);

/**
 * @brief Stack is empty when top is equal to -1.
 *
 * @param stack
 * @return true
 * @return false
 */
bool IsEmpty(Stack *stack);

/**
 * @brief Stack is full when top is equal to the last index.
 *
 * @param stack
 * @return true
 * @return false
 */
bool IsFull(Stack *stack);

#endif
